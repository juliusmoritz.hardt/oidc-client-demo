import { Link } from "react-router-dom";

export default function App() {
  return (
    <div>
      <h1>OIDC Client Demo</h1>
      <nav>
        <Link to="/dashboard">Go to dashboard</Link>
      </nav>
    </div>
  );
}