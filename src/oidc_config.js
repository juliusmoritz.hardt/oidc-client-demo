const configuration = {
    client_id: 'foo',
    redirect_uri: 'http://localhost:3000/authentication/callback',
    response_type: 'code',
    post_logout_redirect_uri: 'http://localhost:3000/',
    scope: 'openid',
    authority: 'http://localhost:3333',
    silent_redirect_uri: 'http://localhost:3000/authentication/silent_callback',
    automaticSilentRenew: false,
    loadUserInfo: false,
};

export default configuration;