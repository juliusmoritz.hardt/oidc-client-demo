import { useReactOidc } from '@axa-fr/react-oidc-context';
import { withAuthentication } from '@axa-fr/react-oidc-context-fetch';
import { useEffect, useState } from 'react';
import oidcConfig from "../oidc_config";

function Dashboard(props) {
    const { oidcUser, logout } = useReactOidc();
    const { profile } = oidcUser;

    const [homegroup, setHomegroup] = useState(null);

    useEffect(() => {
        props.fetch(oidcConfig.authority + '/homegroup.json')
            .then(response => response.json())
            .then(data => setHomegroup(data.group));
    });

    return (
        <div>
            <h1>Dashboard</h1>
            <p>Hello {profile.sub}!</p>
            {homegroup && <p>Your homegroup: {homegroup}</p>}
            <br />
            <button type='button' onClick={logout}>Logout</button>
        </div>
    );
}

export default withAuthentication(window.fetch)(Dashboard);